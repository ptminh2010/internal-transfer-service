package com.bank.transfer.internal.router;

import com.bank.transfer.internal.service.InternalTransferService;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.SagaPropagation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class InternalTransferRouter extends RouteBuilder {
    @Value("${services.limit.reserved}")
    private String TRANFER_ROUTE_NAME;

    @Autowired
    private InternalTransferService internalTransferService;

    @Override
    public void configure() {
        // start from a timer
        from("direct:internal_tranfer_router").routeId(TRANFER_ROUTE_NAME)
                .saga()
                .to("log:DEBUG?showBody=true&showHeaders=true&showProperties=true")
                .compensation("direct:cancel_internal_tranfer_router")
                .log("Executing saga #${header.id} with LRA ${header.Long-Running-Action}")
                .log(" - transfer_id: ${in.headers.transfer_id}")
                .bean(internalTransferService, "buildRequest")
                .to("log:DEBUG?showBody=true&showHeaders=true&showProperties=true")
                //reserved limit
                .to("jms:queue:{{services.limit.reserved}}?exchangePattern=InOut&requestTimeOut={{services.timeout}}" +
                        "&replyTo={{services.limit.reserved}}.reply")
                .log(" - recieve user-limit-info ${body}")
                .to("log:DEBUG?showBody=true&showHeaders=true&showProperties=true")
                //call integration service
                .to("jms:queue:{{services.integration.process}}?exchangePattern=InOut&requestTimeOut={{services.timeout}}" +
                        "&replyTo={{services.integration.process}}.reply")
                .to("log:DEBUG?showBody=true&showHeaders=true&showProperties=true")
                .end();

        from("direct:cancel_internal_tranfer_router")
                .routeId("cancel_internal_tranfer_router")
                .log("Transaction ${header.Long-Running-Action} has been cancelled due to integration fail");
    }

//    @Override
//    public void configure() throws Exception {
//        TransferRequest request = new TransferRequest();
//        request.setAmount(BigDecimal.TEN);
//        request.setBeneficiary("Beneficiary");
//        request.setClientTransactionId(UUID.randomUUID());
//        request.setFromCif("from_cif");
//        // start from a timer
//        from("direct:apiTest").routeId("hello")
//                .setHeader("request", constant(request))
//                .log(" -- body: ${body}")
//                .log("sending message to reserve limit ${in.headers.request.clientTransactionId}")
//                // and call the bean
//                .bean(internalTransferService, "saySomething")
//                // and print it to system out via stream component
//                .to("stream:out");
//    }
}
