package com.bank.transfer.internal.service;

import com.bank.transfer.internal.model.TransferRequest;
import com.bank.transfer.internal.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Body;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class InternalTransferService {
    public String buildRequest(@Body TransferRequest request) {
        return ObjectUtil.objectToString(request);
    }

    public String responseError() {
        return "Error when processing request";
    }
}
